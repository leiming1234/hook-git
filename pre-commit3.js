#!/usr/bin/env node
// 在 commit 之前检查是否有冲突，如果有冲突就 process.exit(1)
const axios = require('axios');
const execSync = require('child_process').execSync

process.on('uncaughtException',(error) => {
    debugger;
    console.error(error);
});

let arg = process.argv;
console.log(arg);


try {
    // git grep 命令会执行 perl 的正则匹配所有满足冲突条件的文件
    let t = execSync(`node ./src/test.js`, {encoding: 'utf-8'});
    console.log(t);
} catch (e) {
    console.log(e)
}

let results = '';

try {
    // git grep 命令会执行 perl 的正则匹配所有满足冲突条件的文件
    results = execSync(`git log -1 --pretty=format:"%ae" --name-status`, {encoding: 'utf-8'})
} catch (e) {
    console.log('没有发现冲突，等待 commit')
    process.exit(0)
}
let list = results.split("\n");

postComponent();

//process.exit(0)

async function postComponent(){
    try{
        const rs = await axios.get('http://localhost:8893/component/test');
        console.log(rs);
    }catch (error){
       console.error(error);
    }
}
